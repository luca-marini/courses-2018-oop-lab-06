package it.unibo.oop.lab06.generics1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GraphImpl<N> implements Graph<N> {

	private Map<N,Set<N>> graph = new HashMap<>();
	
	public GraphImpl() {
	}

	@Override
	public void addNode(N node) {
		if(node != null || !(graph.containsKey(node))) {
			final Set<N> linkedNodes = new HashSet<>();
			this.graph.put(node, linkedNodes);
		}
	}

	@Override
	public void addEdge(N source, N target) {
		if(source != null && target != null ) {
			this.graph.get(source).add(target);
		}
	}

	@Override
	public Set<N> nodeSet() {
		return this.graph.keySet();
	}

	@Override
	public Set<N> linkedNodes(N node) {
		return this.graph.get(node);
	}

	@Override
	public List<N> getPath(N source, N target) {
		// TODO Auto-generated method stub
		return null;
	}

}
