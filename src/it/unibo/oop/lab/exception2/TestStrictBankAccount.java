package it.unibo.oop.lab.exception2;

import org.junit.Test;


import org.junit.Assert;


/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {
	
	private final double INITIAL_AMOUNT = 10000;

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	
    	AccountHolder marioR = new AccountHolder("Mario", "Rossi", 1);
    	AccountHolder lucaM = new AccountHolder("Luca", "Marini", 2);
    	StrictBankAccount acc1 = new StrictBankAccount(1, INITIAL_AMOUNT, 10);
    	StrictBankAccount acc2 = new StrictBankAccount(2, INITIAL_AMOUNT, 10);
    	
    	acc1.deposit(1, 1000);
    	System.out.println(acc1.getBalance());
    	
    	acc2.withdraw(2, 1000);
    	System.out.println(acc2.getBalance());
    	
    	try{
    		acc2.withdrawFromATM(2, 9999999);
    		Assert.fail();
    	} catch(NotEnoughFoundsException e) {
    		Assert.assertNotNull(e);
    		throw e;
    	}
    	
    	
    	for(int i = 0; i < 10; i++) {
    		acc1.withdraw(1, 1);
    	}
    	try {
    		acc1.withdrawFromATM(1, 1000);
    		Assert.fail();
    	} catch(TransactionsOverQuotaException e) {
    		Assert.assertNotNull(e);
    		throw e;
    	}
    	
    	try{
    		acc1.withdraw(32, 100);
    		Assert.fail();
    	}catch(WrongAccountHolderException e) {
    		Assert.assertNotNull(e);
    		throw e;
    	}
    }
}
