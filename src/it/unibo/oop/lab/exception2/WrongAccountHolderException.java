package it.unibo.oop.lab.exception2;

public class WrongAccountHolderException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WrongAccountHolderException() {
	}
	
	public String getMessage() {
		return "Can not access, Wrong Account Holder!";
	}

}
