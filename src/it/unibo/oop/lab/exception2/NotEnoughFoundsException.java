package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final double amount;

	public NotEnoughFoundsException(final double amount) {
		this.amount = amount;
	}
	
	public String getMessage() {
		return "Can not withdraw " + this.amount + " money, not enough in your account";
	}

}
