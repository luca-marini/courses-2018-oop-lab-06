package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {
	
	private static final int ELEMS = 1000000;
	private static final int TO_MS = 1000000;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
    	
    	
    	List<Integer> l = new ArrayList<>();
    	for(int i = 1000; i<2000; i++) {
    		l.add(i);
    	}
    	
    	List<Integer> l2 = new LinkedList<>(l);
    	
    	int i = l.get(l.size()-1);
    	l.set(l.size()-1, l.get(0));
    	l.set(0, i);
    	
    	for(int i1 : l) {
    		System.out.println(i1);
    	}
    	long time1 = System.nanoTime();
    	for (int j = 1; j <= ELEMS; j++) {
            l2.set(0, j);
        }
    	
    	time1 = System.nanoTime() - time1;
        System.out.println("Adding " + ELEMS
                + " in a Linked List took " + time1
                + "ns (" + time1 / TO_MS + "ms)");
    	
    	
    	long time2 = System.nanoTime();
    	for (int j = 1; j <= ELEMS; j++) {
            l.set(0, j);
        }
    	
    	
    	time2 = System.nanoTime() - time2;
        System.out.println("Adding " + ELEMS
                + " in a Array List took " + time2
                + "ns (" + time2 / TO_MS + "ms)");
    	
        
        
        long timereadlinked = System.nanoTime();
        for (int j = 1; j <= 1000; j++) {
            l2.get(l2.size()/2);
        }
        
        timereadlinked = System.nanoTime() - timereadlinked;
        System.out.println("Reading " + ELEMS
                + " in a Array List took " + timereadlinked
                + "ns (" + timereadlinked / TO_MS + "ms)");
       
        
        
        
        long timereadarraylist = System.nanoTime();
        for (int j = 1; j <= 1000; j++) {
            l.get(l2.size()/2);
        }
        
        timereadarraylist = System.nanoTime() - timereadarraylist;
        System.out.println("Reading " + ELEMS
                + " in a Array List took " + timereadarraylist
                + "ns (" + timereadarraylist / TO_MS + "ms)");
    	
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
        
        Map<String, Long> world = new TreeMap<>();
        
        world.put("Africa", (long) 1110635000);
        world.put("Americas", (long) 972005000);
        world.put("Antarctica", (long) 0);
        world.put("Asia", (long) 4_298_723_000L);
        world.put("Europe", (long) 742452000);
        world.put("Oceania", (long) 38304000);
        
        long res=0;
        
        for(Long num : world.values()) {
        	res= res + num;
        }
        System.out.println(res);
        
    }
}
