package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int x;
    private final int y;
    
	public NotEnoughBatteryException(final int initX, final int initY) {
		this.x = initX;
		this.y = initY;
	}
	
	public String getMessage() {
	    return "Can not move to pos(" + this.x + ", " + this.y + "), battery empty";
	}

}
